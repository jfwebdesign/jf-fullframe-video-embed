<?php
if( current_user_can('edit_others_pages') ) {
	global $wpdb;
	$apps_table = $wpdb->prefix . "jf_video_embed";
	
if(isset($_POST['add_video']))
{
	$wpdb->get_results("SELECT id FROM ".$apps_table." WHERE videoURL = '".$_POST['videoURL']."'");
	if($wpdb->num_rows > 0)
		{
			echo "<h2>That video is already listed.</h2>";
		}
	else
		{
	//Strip for embed
		//Vimeo
		if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $_POST['videoURL'], $regs)) {
            $newID = $regs[3];
        }
		//YouTube
		if (preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $_POST['videoURL'], $regs)) {
            $newID = $regs[1];
        }
			
		if ($_POST['autoplay'] == 1){$muteIt = '1';}else{$muteIt = $_POST['mute'];}
	//insert member data into database
   	    $wpdb->get_results("INSERT INTO ".$apps_table." (videoURL, videoTitle, autoplay, videoButton1text, videoButton1link, videoButton2text, videoButton2link, mobileImage,originalURL, mute, vloop, startSeconds, endSeconds) VALUES ('".$newID."','".$_POST['videoTitle']."','".$_POST['autoplay']."','".$_POST['videoButton1text']."','".$_POST['videoButton1link']."','".$_POST['videoButton2text']."','".$_POST['videoButton2link']."','".$_POST['mobileImage']."','".$_POST['videoURL']."','".$muteIt."','".$_POST['vloop']."','".$_POST['startSeconds']."','".$_POST['endSeconds']."')");
			echo "<h2>Video added.</h2>";
		}

}//closes the add
?>	
	<form method="post" action="" id="" class="print_page">
	<table width="75%">
	<table width="75%">
	<tr>
		<td><p>Video Title:</p></td><td><input type="text" size="50" id="video_title" name="videoTitle" value="" /></td>
	</tr>
	<tr>
		<td><p>Video URL:</p></td><td><input type="text" size="50" id="video_url" name="videoURL" value="" /></td>
	</tr>
	<tr>
		<td><p>Loop:</p></td>
		<td>
			<select name="vloop">
				<option value="0" selected>Off</option>
				<option value="1">On</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><p>Autoplay:</p></td>
		<td>
			<select name="autoplay">
				<option value="0" selected>Off</option>
				<option value="1">On</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><p>Mute:</p></td>
		<td>
			<select name="mute">
				<option value="0" selected>Off</option>
				<option value="1">On</option>
			</select>
			<p>*if autoplay is on, muted MUST be on.</p>
		</td>
	</tr>
    <tr>
		<td><p>YouTube Start Seconds:</p></td><td><input type="text" size="50" id="startSeconds" name="startSeconds" value="" /></td>
	</tr>
        <tr>
		<td><p>YouTube End Seconds:</p></td><td><input type="text" size="50" id="endSeconds" name="endSeconds" value="" /></td>
	</tr>
    <tr>
		<td><p>Top Button Text:</p></td><td><input type="text" size="50" id="videoButton1text" name="videoButton1text" value="" /></td>
	</tr>
    <tr>
		<td><p>Top Button Link:</p></td><td><input type="text" size="50" id="videoButton1link" name="videoButton1link" value="" /></td>
	</tr>
    <tr>
		<td><p>Bottom Button Text:</p></td><td><input type="text" size="50" id="videoButton2text" name="videoButton2text" value="" /></td>
	</tr>
    <tr>
		<td><p>Bottom Button Link:</p></td><td><input type="text" size="50" id="videoButton2link" name="videoButton2link" value="" /></td>
	</tr>
	<tr>
		<td><p><label for="mobileImage"><?php _e( 'Image for Mobile' ); ?></label></p></td>
      		<td style="display: flex;"><input id="mobileImage" name="mobileImage" type="text" value="" /><button class="upload_image_button button button-primary">Select Images</button>
		</td>
	</tr>
	<tr>
		<td><input name="add_video" type="submit" id="Submit" value="Add Video" /></p></td>
	</tr>
	</table>
	</form>	
		
<?php	

}
?>