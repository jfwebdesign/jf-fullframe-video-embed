<!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
<div id="player"></div>
<script>
// Load the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var videoId = '<?php echo $video->videoURL; ?>';
    var mute = '<?php echo $video->mute; ?>';
    var loop = '<?php echo $video->vloop; ?>';
    var autoplay = '<?php echo $autoplay; ?>'; 
    var startSeconds = '<?php echo $video->startSeconds; ?>';
    var endSeconds = '<?php echo $video->endSeconds; ?>';

  // 2. This code loads the IFrame Player API code asynchronously.
  var tag = document.createElement('script');

  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // 3. This function creates an <iframe> (and YouTube player)
  //    after the API code downloads.
  var player;
  function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        width: '1920',
        height: '1080',
        videoId: videoId,
        playerVars: {
            controls: 0, // Show pause/play buttons in player
            showinfo: 1, // Hide the video title
            modestbranding: 1, // Hide the Youtube Logo
            fs: 1, // Hide the full screen button
            cc_load_policy: 0, // Hide closed captions
            iv_load_policy: 3, // Hide the Video Annotations
            start: startSeconds,
            end: endSeconds,
            autohide: 0, // Hide video controls when playing
            loop: <?php echo $video->vloop; ?>,
            mute: <?php echo $video->mute; ?>,
            rel: 0,
            disablekb: 1,
            enablejsapi: 1,
            autoplay: <?php echo $video->autoplay; ?>,
            playlist: videoId,
            playsinline: 1
        },
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onStateChange2
      }
    });
  }
function onStateChange2(state) {
    if (state.data === YT.PlayerState.CUED) {
        player.seekTo(startSeconds);
    }
    if (state.data === YT.PlayerState.BUFFERING) {
        player.seekTo(startSeconds);
    }
    if (state.data === YT.PlayerState.ENDED) {
        player.seekTo(startSeconds);
    }
    if (state.data === YT.PlayerState.ENDED) {
        player.seekTo(startSeconds);
    }
}

  // 4. The API will call this function when the video player is ready.
  function onPlayerReady(event) {
     event.target.mute();
    event.target.playVideo();
  }

</script>  
