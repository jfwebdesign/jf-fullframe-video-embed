<?php
/**
 * Plugin Name: JF Fullframe Video Embed
 * Plugin URI: http://jfwebdesign.com
 * Description: Add YouTube or Vimeo videos to pages through shortcode.
 * Version: 3.2
 * Author: JF WebDesign
 * Author URI: http://jfwebdesign.com
 */
$plugin_url = 'jf-video-embed/';

function rel_video_app(){
	global $wpdb;

	//include the wordpress db functions
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		
	//create the name of the table including the wordpress prefix 
	$apps_table = $wpdb->prefix . "jf_video_embed";
	//$wpdb->show_errors(); 
	
	//check if there are any tables of that name already
	if($wpdb->get_var("show tables like '$apps_table'") !== $apps_table) 
	{  
		$sql = "CREATE TABLE $apps_table (
			videoID int(11) NOT NULL AUTO_INCREMENT,
			videoURL VARCHAR(500) NOT NULL,
			videoTitle VARCHAR(500) NOT NULL,
			videoButton1text VARCHAR(500) NOT NULL,
			videoButton1link VARCHAR(500) NOT NULL,
			videoButton2text VARCHAR(500) NOT NULL,
			videoButton2link VARCHAR(500) NOT NULL,
			vloop int(11) NOT NULL,
            startSeconds int(11) NOT NULL,
            endSeconds int(11) NOT NULL,
			autoplay int(11) NOT NULL,
			mute int(11) NOT NULL,
			mobileImage VARCHAR(500) NOT NULL,
			originalURL VARCHAR(500) NOT NULL,
		  UNIQUE KEY ID (videoID)
		);";
  dbDelta($sql);
  }	
	
	
	
	//register the new table with the wpdb object
	if (!isset($wpdb->jf_video_embed)) 
	{
		$wpdb->jf_video_embed = $apps_table; 
		//add the shortcut so you can use $wpdb->profile_apps
		$wpdb->tables[] = str_replace($wpdb->prefix, '', $apps_table); 
	}
} 
//add to front and backend inits
add_action('init', 'rel_video_app');
// Hook for adding admin menus
add_action('admin_menu', 'add_video_menu');
function add_video_menu() {
    // Manage Videos:
    add_menu_page(__('Manage Videos','video_1'), __('Manage Videos','video_1'), 'manage_options', 'endor-top-level-handle', 'endor_toplevel_page' );
	// Add
	add_submenu_page('endor-top-level-handle', __('Add Video','video_2'), __('Add Video','video_2'), 'manage_options', 'add_video', 'endor_add_video');
	//Edit
	add_submenu_page(NULL, __('Edit Video','video_3'), __('Edit Video','video_3'), 'manage_options', 'edit_video', 'endor_edit_video');
}

// Manage Videos back end page
function endor_toplevel_page() {
    echo "<h2>" . __( 'Videos', 'video_1' ) . "</h2>";
	//Plugin Pieces
	require_once( plugin_dir_path( __FILE__ ) . 'jf-video-embed-info.php' );
}
// Add Videos back end page
function endor_add_video() {
    echo "<h2>" . __( 'Add Videos', 'video_2' ) . "</h2>";
	//Plugin Pieces
	require_once( plugin_dir_path( __FILE__ ) . 'jf-video-embed-add.php' );
}
// Edit Videos back end page
function endor_edit_video() {
    echo "<h2>" . __( 'Edit Videos', 'video_3' ) . "</h2>";
	//Plugin Pieces
	require_once( plugin_dir_path( __FILE__ ) . 'jf-video-embed-edit.php' );
}
require_once( plugin_dir_path( __FILE__ ) . 'jf-video-embed-shortcode.php' );
function jf_video_embed_script() {   
	wp_enqueue_script( 'media-upload' );
	wp_enqueue_media();
	wp_enqueue_script( 'jf_video_embed_script', plugin_dir_url( __FILE__ ) . 'jf_video_embed_script.js', array('jquery'), '1.0' );
}
add_action('admin_enqueue_scripts', 'jf_video_embed_script');
?>