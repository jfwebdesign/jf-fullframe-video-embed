<?php
//add button in shortcode
add_shortcode('jf_video_embed', 'load_jf_video_embed'); // [jf_video_embed]
function load_jf_video_embed($atts)
{
	// Attributes
	extract( shortcode_atts(
		array(
			'videoid' => ''
		), $atts )
	);
	$attributes = array($atts); 
	foreach ($attributes as $value) {
		$videoid = $value['videoid'];
	}
	
	global $wpdb;
	$apps_table = $wpdb->prefix . "jf_video_embed";
	$get_videos = $wpdb->get_results("SELECT * FROM ".$apps_table ." where videoid = ".$videoid.";");
	$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
{$device = 'mobile';} else {$device = 'not_mobile';}
ob_start();

	echo '<style>
	.jf_videos
		{
			position: relative;
			width: 100%;
    		height: 56.25vw;
			display: flex;
			justify-content: center;
			align-items: center; 
		}
	.jf_videos iframe 
		{
			margin: 0 !important;
			width: 100%;
			height: 100%;
			border: 0;
		}
		.jf_videos > .inner-wrap {
			position:absolute;
			z-index: 1000;
		}
		.overlayImage {
			position:absolute;
			z-index: 900;
		}
		.jf_videos > .inner-wrap .video-title {
			position:relative;
			margin: 0 auto;
			font-size: 4rem;
			color: #ffffff;
			text-align: center;
		}
		.video-buttons {
			display: block;
		}
		.jf_videos > .inner-wrap a .video-button-1, .jf_videos > .inner-wrap a .video-button-2
		{
			position:relative;
			margin: 0 auto 2%;
			font-size: 2rem;
			color: #ffffff;
			text-align: center;
			border-radius: 8px;
			padding: 10px 15px;
			border: 1px solid #ffffff;
			width: 250px;
		}
		.jf_videos >.inner-wrap a .video-button-1
		{
			background-color: #EC1C24; /* Red*/
		}
		.jf_videos > .inner-wrap a .video-button-2
		{
			background-color: #1B75BB; /* Blue*/
		}	
		.inner-wrap a {text-decoration: none !important; border: 0px !important; color: transparent;}
		.mobileImage {width: 100%; height: 100%; display: block !important;} 
		@media screen and (max-width: 909px) 
		{
            .jf_videos > .inner-wrap 
                {
                    position:absolute;
                    margin: auto;
                    z-index: 1000;
                    width: 100%;
                    padding: 0 0 2% 0;
                }	
            .video-buttons {
                display: flex;
                justify-content: center;
                }
            .jf_videos > .inner-wrap .video-title {
                    position:relative;
                    margin: 0 auto;
                    font-size: 3rem;
                    color: #ffffff;
                    text-align: center;
                }
            .jf_videos > .inner-wrap a .video-button-1, .jf_videos > .inner-wrap a .video-button-2
                {
                    position:relative;
                    margin: 0 20px;
                    font-size: 1rem;
                    color: #ffffff;
                    text-align: center;
                    border-radius: 8px;
                    padding: 10px 15px;
                    border: 1px solid #ffffff;
                    width: 150px;
                }
            .jf_videos
                {
                    /*height: 25vh;*/
                    padding-top: 0%;
                }
        }
                ';
	echo '</style>';
		foreach($get_videos as $video) 
		{	
		$autoplay = $video->autoplay;
		
		$input_line = $video->originalURL;
		$url = preg_match('/(www\.)?vimeo\.com|youtu\.be|youtube\.com/', $input_line, $output_array);
		if ($video->mobileImage !=='') 
			{
				$image_attributes = wp_get_attachment_image_src( $attachment_id = $video->mobileImage, 'full' );
				if ( $image_attributes ) :
					$mobileImage = $image_attributes[0];
				endif;
			}
        if($output_array[0] == 'youtu.be' || $output_array[0] == 'youtube.com') {
            $source = 'youtube';
        } 
        elseif($output_array[0] == 'vimeo.com') {
            $source = 'vimeo';
        } 
		echo '<div class="jf_videos">';			
            if ($video->autoplay == '1'){echo "<div class='overlayImage'><img src='".plugin_dir_url( __FILE__ ) . "inc/overlay.png'></div>";}
			if ($source == 'youtube') {	
				//if ($device !== 'mobile') {			
					echo '<div class="inner-wrap">';					
						if ($video->videoTitle !=='') { echo '<div class="video-title">'.$video->videoTitle.'</div>';}
						if ($video->videoButton1link !=='' || $video->videoButton2link !=='') { 
							if(strpos($video->videoButton1link, 'starpal.org') !== false) {$target1 = "_self";} else {$target1 = "_blank";}
							if(strpos($video->videoButton2link, 'starpal.org') !== false) {$target2 = "_self";} else {$target2 = "_blank";}
							echo '<div class="video-buttons">';
								if ($video->videoButton1text !== ''){echo '<a href="'.$video->videoButton1link.'" target="'.$target1.'"><div class="video-button-1">'.$video->videoButton1text.'</div></a>';}
								if ($video->videoButton2text !== ''){echo '<a href="'.$video->videoButton2link.'" target="'.$target2.'"><div class="video-button-2">'.$video->videoButton2text.'</div></a>';}
							echo "</div>";
						}
					echo "</div>";
                    require_once( plugin_dir_path( __FILE__ ) . 'jf-video-embed-youtube.php' );
                    //echo '<iframe class="youtube" src="https://www.youtube.com/embed/'.$video->videoURL.'?rel=0&modestbranding=1&autohide=1&mute='.$video->mute.'&showinfo=0&controls=0&disablekb=1&enablejsapi=1&autoplay='.$autoplay.'&loop='.$video->vloop.'" frameborder="0" allow="autoplay;" allowfullscreen ></iframe>';
               // }
			/*	
                if ($device == 'mobile') {
					echo '<div class="inner-wrap mobile">';
						if ($video->videoTitle !=='') { echo '<div class="video-title">'.$video->videoTitle.'</div>';}
						if ($video->videoButton1link !=='' || $video->videoButton2link !=='') { 
							if(strpos($video->videoButton1link, 'starpal.org') !== false) {$target1 = "_self";} else {$target1 = "_blank";}
							if(strpos($video->videoButton2link, 'starpal.org') !== false) {$target2 = "_self";} else {$target2 = "_blank";}
							echo '<div class="video-buttons">';
								if ($video->videoButton1text !== ''){echo '<a href="'.$video->videoButton1link.'" target="'.$target1.'"><div class="video-button-1">'.$video->videoButton1text.'</div></a>';}
								if ($video->videoButton2text !== ''){echo '<a href="'.$video->videoButton2link.'" target="'.$target2.'"><div class="video-button-2">'.$video->videoButton2text.'</div></a>';}
							echo "</div>";
							;
						}
					echo "</div>";
					//if ($video->videoButton1link !== '' && $video->mobileImage !=='' || $video->videoButton2link !== '' && $video->mobileImage !=='') { echo '<div class="mobileImage" style="background-image:url('.$mobileImage.'); background-repeat: no-repeat; background-size: cover;"></div>';}
					//if ($video->videoButton1link == '' && $video->videoButton2link == '' || $video->mobileImage =='') { 
				    require_once( plugin_dir_path( __FILE__ ) . 'jf-video-embed-youtube.php' );
                    //echo '<iframe class="youtube" src="https://www.youtube.com/embed/'.$video->videoURL.'?rel=0&modestbranding=1&autohide=1&mute='.$video->mute.'&showinfo=0&controls=0&autoplay=0&loop='.$video->vloop.'&playlist='.$video->videoURL.'" frameborder="0" allow="autoplay;" allowfullscreen ></iframe>';
                    //}
				}
                */
			} 
            elseif ($source ==  'vimeo') {
				echo '<div class="inner-wrap">';
						if(strpos($video->videoButton1link, 'starpal.org') !== false) {$target1 = "_self";} else {$target1 = "_blank";}
						if(strpos($video->videoButton2link, 'starpal.org') !== false) {$target2 = "_self";} else {$target2 = "_blank";}
						if ($video->videoTitle !=='') { echo '<div class="video-title">'.$video->videoTitle.'</div>';}
						if ($video->videoButton1link !=='' || $video->videoButton2link !=='') { 
							echo '<div class="video-buttons">';
								if ($video->videoButton1text !== ''){echo '<a href="'.$video->videoButton1link.'" target="'.$target1.'"><div class="video-button-1">'.$video->videoButton1text.'</div></a>';}
								if ($video->videoButton2text !== ''){echo '<a href="'.$video->videoButton2link.'" target="'.$target2.'"><div class="video-button-2">'.$video->videoButton2text.'</div></a>';}
							echo "</div>";
						}
					echo "</div>";
				echo '<iframe class="vimeo" src="https://player.vimeo.com/video/'.$video->videoURL.'?background='.$autoplay.'&autoplay='.$autoplay.'&loop='.$video->vloop.'&muted='.$video->mute.'" width="640" height="480" frameborder="0" allow="autoplay;" ></iframe>';
			}
			else {echo 'Video must be hosted on vimeo or youtube. ' .$source;}
		echo "</div>";
		}
	
return ob_get_clean();
}