<?php
if( current_user_can('edit_others_pages') ) {
	global $wpdb;
	$apps_table = $wpdb->prefix . "jf_video_embed";
	
if(isset($_POST['update']))
{
	echo '<b>Video Updated</b><br><br>';
	$videoID = $_POST['videoID'];

global $wpdb;
	//Strip for embed
		//Vimeo
	$newID = $_POST['videoURL'];
		if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $_POST['videoURL'], $regs)) {
            $newID = $regs[3];
        }
		//YouTube
		if (preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $_POST['videoURL'], $regs)) {
            $newID = $regs[1];
        }
	if ($_POST['autoplay'] == 1){$muteIt = '1';}else{$muteIt = $_POST['mute'];}
$wpdb->update($apps_table, array( 
	'videoTitle' => $_POST['videoTitle'],
	'videoURL' => $newID,
	'videoButton1text' => $_POST['videoButton1text'],
	'videoButton1link' => $_POST['videoButton1link'],
	'videoButton2text' => $_POST['videoButton2text'],
	'videoButton2link' => $_POST['videoButton2link'],
	'vloop' => $_POST['vloop'],
    'startSeconds' => $_POST['startSeconds'],
    'endSeconds' => $_POST['endSeconds'],
	'autoplay' => $_POST['autoplay'],
	'mute' => $muteIt,
	'mobileImage' => $_POST['mobileImage'],
	'originalURL' => $_POST['videoURL'],
	),

	array( 'videoID' => $videoID ) );

}//closes the update
	
//delete logic button bit
	if(isset($_POST['jf_video_delete'])){
	$delete_id = $_POST['videoID'];
	
	global $wpdb;
	$q = "DELETE FROM ".$apps_table." WHERE videoID ='".$delete_id."'";
	$jf_video_delete = $wpdb->query($q);
		
		
		if (count($jf_video_delete)!==0){
		echo "<h2>Record Deleted</h2> "; 
		
		} else	{
		echo 'Something went wrong.';
		}
		}
	
if(isset($_REQUEST['eid'])){
	$searchID = $_REQUEST['eid'];
}

	$get_names = $wpdb->get_results("SELECT * FROM ".$apps_table . " where videoID ='".$searchID."'");
		
if (count($get_names)===0){
	echo "<h2>The video could not be found.</h2> ";
} 
else	
{
			
	foreach($get_names as $name){ ?> 
<div style="margin: 0 auto; width: 100%; text-align: center;">

<?php
		$autoplay = '0';
		$input_line = $name->originalURL;
			$url = preg_match('/(www\.)?vimeo\.com|youtu\.be|youtube\.com/', $input_line, $output_array);
			if($output_array[0] == 'youtu.be' || $output_array[0] == 'youtube.com') {
			$source = 'youtube';

			} 
			elseif($output_array[0] == 'vimeo.com') {


			$source = 'vimeo';

			} 
				
			if ($source == 'youtube') {
				echo '<iframe src="https://www.youtube.com/embed/'.$name->videoURL.'?rel=0&modestbranding=1&autohide=1&mute='.$mute.'&showinfo=0&controls=0&autoplay='.$autoplay.'&loop='.$name->vloop.'&playlist='.$name->videoURL.'" frameborder="0" allow="autoplay;" allowfullscreen></iframe>';
			
			} elseif ($source ==  'vimeo') {
				echo '<iframe src="https://player.vimeo.com/video/'.$name->videoURL.'?autoplay='.$autoplay.'&loop='.$name->vloop.'&muted='.$mute.'" width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>';
			}
			else {echo 'Video must be hosted on vimeo or youtube. ' .$source;}
				
				
			
		if ($name->mobileImage !=='') {
			$image_attributes = wp_get_attachment_image_src( $attachment_id = $name->mobileImage, 'large' );
				if ( $image_attributes ) :
					$mobileImage = $image_attributes[0];
				endif;
		}							 
?>
	
	
</div>	
<div>

	<form method="post" action="" id="" class="print_page">
	<input type="hidden" size="50" id="videoID" name="videoID" value="<?php echo $name->videoID; ?>" />
	<table width="75%">
	<tr>
		<td><p>Video Title:</p></td><td><input type="text" size="50" id="video_title" name="videoTitle" value="<?php echo $name->videoTitle; ?>" /></td>
	</tr>
	<tr>
		<td><p>Video URL:</p></td><td><input type="text" size="50" id="video_url" name="videoURL" value="<?php echo $name->originalURL; ?>" /></td>
	</tr>
	<tr>
		<td><p>Loop:</p></td>
		<td>
			<select name="vloop">
				<option value="0" <?php if ($name->vloop != 1){echo 'selected';} ?>>Off</option>
				<option value="1" <?php if ($name->vloop == 1){echo 'selected';} ?>>On</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><p>Autoplay:</p></td>
		<td>
			<select name="autoplay">
				<option value="0" <?php if ($name->autoplay != 1){echo 'selected';} ?>>Off</option>
				<option value="1" <?php if ($name->autoplay == 1){echo 'selected';} ?>>On</option>
			</select>
		</td>
	</tr>
		<tr>
		<td><p>Mute:</p></td>
			<td>
				<select name="mute">
					<option value="0" <?php if ($name->mute != 1){echo 'selected';} ?>>Off</option>
					<option value="1" <?php if ($name->mute == 1){echo 'selected';} ?>>On</option>
				</select>
				<p>*if autoplay is on, muted MUST be on.</p>
			</td>
	</tr>
    <tr>
		<td><p>YouTube Start Seconds:</p></td><td><input type="text" size="50" id="startSeconds" name="startSeconds" value="<?php echo $name->startSeconds; ?>" /></td>
	</tr>
        <tr>
		<td><p>YouTube End Seconds:</p></td><td><input type="text" size="50" id="endSeconds" name="endSeconds" value="<?php echo $name->endSeconds; ?>" /></td>
	</tr>
		<tr>
		<td><p>Top Button Text:</p></td><td><input type="text" size="50" id="videoButton1text" name="videoButton1text" value="<?php echo $name->videoButton1text; ?>" /></td>
	</tr>
		<tr>
		<td><p>Top Button Link:</p></td><td><input type="text" size="50" id="videoButton1link" name="videoButton1link" value="<?php echo $name->videoButton1link; ?>" /></td>
	</tr>
		<tr>
		<td><p>Bottom Button Text:</p></td><td><input type="text" size="50" id="videoButton2text" name="videoButton2text" value="<?php echo $name->videoButton2text; ?>" /></td>
	</tr>
		<tr>
		<td><p>Bottom Button Link:</p></td><td><input type="text" size="50" id="videoButton2link" name="videoButton2link" value="<?php echo $name->videoButton2link; ?>" /></td>
	</tr>
		</tr>
	<tr>
		<td><p><label for="mobileImage"><?php _e( 'Image for Mobile' ); ?></label></p>
      		<td style="display: flex;">
				<div class="mobileImage" style="margin-right: 10px;"><img src="<?php echo $mobileImage; ?>" width=100px></div>
				<input id="mobileImage" name="mobileImage" type="text" value="<?php echo $name->mobileImage;?>" style="margin-right: 10px;" />
				<button class="upload_image_button button button-primary">Select Images</button>
			</td>
		</td>
	</tr>
	<tr>
		<td><input name="update" type="submit" id="Submit" value="Update" /></p></td>
	</tr>
	</table>
	</form>	
	<hr>
	<form method="post" action="">
	<input type="hidden" name="videoID" id="videoID" value='<?php echo $name->videoID; ?>' />
	<input type="submit" name="jf_video_delete" value="Delete Video" onclick="return confirm('Are you sure?')"/>
	</form>
</div>
<?php	
			} //Closes foreach
} //Closes else
}
?>